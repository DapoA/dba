<#
# Installs SQL Server 2019 ****Amended from 2018/4/5 Niall Brady, https://www.windows-noob.com
#
# This script:            Installs SQL Server 2019, SSMS and Setup Scripts
# Before running:         Edit the variables as necessary (lines 17-79). Copy the SQL Service Management Studio (SSMS-Setup-ENU.exe) and Service Pack files to $folderpath\ in advance if you don't want the script to download the exe's
# Usage: Run this script on the ConfigMgr Primary Server as a user with local Administrative permissions on the server
#>
  If (-NOT ([Security.Principal.WindowsPrincipal] [Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole(`
        [Security.Principal.WindowsBuiltInRole] "Administrator"))

    {
        Write-Warning "You do not have Administrator rights to run this script! Please re-run this script as an Administrator!"
        Break
    }

#Set folder locations (DEFAULT VALUES)
$SQLUSERDBDIR="H:\Data"
$SQLUSERDBLOGDIR="G:\Log"
$SQLTEMPDBDIR="I:\MSSQL\Data"
$SQLBACKUPDIR="F:\DBBackup"

# below variables are customizable
$folderpath="E:\Install"
# $inifile="$folderpath\ConfigurationFile.ini"

# next line sets user as a SQL sysadmin
$yourusername="CUBECLOUD\CUBE_SQL_DEVOPS_ADMIN"

#Path where the installation media is mounted
$SQLsource="D:" 
$SQLInstallDrive = "E:"

# configurationfile.ini settings https://msdn.microsoft.com/en-us/library/ms144259.aspx
$ACTION="Install"
$ErrorReporting="False"
$SUPPRESSPRIVACYSTATEMENTNOTICE="False"
$IACCEPTROPENLICENSETERMS="False"
$ENU="True"
$QUIET="True"
$QUIETSIMPLE="False"
$UpdateEnabled="True"
$USEMICROSOFTUPDATE="False"
$FEATURES="SQLENGINE,REPLICATION,IS" #,RS,IS,SDK,FULLTEXT

#Change to reflect correct Directory e.g E:\Install
$UpdateSource="E:\Install"
$HELP="False"
$INDICATEPROGRESS="False"
$X86="False"

##DEFAULT IS MSSQLSERVER
$INSTANCENAME="DEV2SQL01" 
$INSTALLSHAREDDIR="$SQLInstallDrive\Program Files\Microsoft SQL Server"
$INSTALLSHAREDWOWDIR="$SQLInstallDrive\Program Files (x86)\Microsoft SQL Server"
$INSTANCEDIR="$SQLUSERDBDIR"
$AGTSVCSTARTUPTYPE="Automatic"
$ISSVCSTARTUPTYPE="Manual"
#$ISSVCACCOUNT="NT AUTHORITY\System"
$SQLSVCSTARTUPTYPE="Automatic"

#Set service accounts
$d= Get-Credential 'DBE service account'
$SQLSVCACCOUNT=$d.UserName
$SQLSVCPASSWORD=$d.GetNetworkCredential().Password

$a= Get-Credential 'AGT service account'
$AGTSVCACCOUNT=$a.UserName
$AGTSVCPASSWORD=$a.GetNetworkCredential().Password

$b= Get-Credential 'SSIS service account'
$ISSVCACCOUNT=$b.UserName
$ISSVCPASSWORD=$b.GetNetworkCredential().Password

#Change Collation as appropriate
$SQLCOLLATION="Latin1_General_CI_AS"

$SQLSVCINSTANTFILEINIT="True"
$SQLSYSADMINACCOUNTS="$yourusername"
$SQLTEMPDBFILECOUNT="4"
$SQLTEMPDBFILESIZE="1024"
$SQLTEMPDBFILEGROWTH="1024"
$SQLTEMPDBLOGFILESIZE="1024"
$SQLTEMPDBLOGFILEGROWTH="1024"
$ADDCURRENTUSERASSQLADMIN="True"
$TCPENABLED="1"
$NPENABLED="1"
$BROWSERSVCSTARTUPTYPE="Disabled"
$RSSVCACCOUNT="NT AUTHORITY\System"
$RSSVCSTARTUPTYPE="Automatic"
$IAcceptSQLServerLicenseTerms="True"

#create SQL folders
Write-Host "Creating SQL directories..."
New-Item -ItemType directory -Path $SQLUSERDBDIR
New-Item -ItemType directory -Path $SQLUSERDBLOGDIR
New-Item -ItemType directory -Path $SQLTEMPDBDIR
New-Item -ItemType directory -Path $SQLBACKUPDIR
New-Item -ItemType directory -Path $SQLARCHIVEDBACKUPS
#New-Item -ItemType directory -Path $SQLMAINTENANCELOGS

# do not edit below this line
$conffile= @"
[OPTIONS]
Action="$ACTION"
ErrorReporting="$ERRORREPORTING"
Quiet="$Quiet"
Features="$FEATURES"
InstanceName="$INSTANCENAME"
InstanceDir="$INSTANCEDIR"
SQLSVCAccount="$SQLSVCACCOUNT"
SQLSysAdminAccounts="$SQLSYSADMINACCOUNTS"
SQLSVCStartupType="$SQLSVCSTARTUPTYPE"
AGTSVCACCOUNT="$AGTSVCACCOUNT"
AGTSVCSTARTUPTYPE="$AGTSVCSTARTUPTYPE"
RSSVCACCOUNT="$RSSVCACCOUNT"
RSSVCSTARTUPTYPE="$RSSVCSTARTUPTYPE"
ISSVCACCOUNT="$ISSVCACCOUNT" 
ISSVCSTARTUPTYPE="$ISSVCSTARTUPTYPE"
SQLCOLLATION="$SQLCOLLATION"
TCPENABLED="$TCPENABLED"
NPENABLED="$NPENABLED"
IAcceptSQLServerLicenseTerms="$IAcceptSQLServerLicenseTerms"
"@


# Check for Script Directory & file
if (Test-Path "$folderpath"){
 write-host "The folder '$folderpath' already exists, will not recreate it."
 } else {
mkdir "$folderpath"
}
if (Test-Path "$folderpath\ConfigurationFile.ini"){
 write-host "The file '$folderpath\ConfigurationFile.ini' already exists, removing..."
 Remove-Item -Path "$folderpath\ConfigurationFile.ini" -Force
 } else {

}
# Create file:
write-host "Creating '$folderpath\ConfigurationFile.ini'..."
New-Item -Path "$folderpath\ConfigurationFile.ini" -ItemType File -Value $Conffile

# Configure Firewall settings for SQL

write-host "Configuring SQL Server 2019 Firewall settings..."

#Enable SQL Server Ports

New-NetFirewallRule -DisplayName "SQL Server" -Direction Inbound -Protocol TCP -LocalPort 1433 -Action allow
New-NetFirewallRule -DisplayName "SQL Listener Ports" -Direction Inbound -Protocol TCP -LocalPort 10001-10010 -Action allow
New-NetFirewallRule -DisplayName "SQL Always On" -Direction Inbound -Protocol TCP -LocalPort 5020-5030 -Action allow
New-NetFirewallRule -DisplayName "SQL Admin Connection" -Direction Inbound -Protocol TCP -LocalPort 1434 -Action allow
New-NetFirewallRule -DisplayName "SQL Database Management" -Direction Inbound -Protocol UDP -LocalPort 1434 -Action allow
New-NetFirewallRule -DisplayName "SQL Service Broker" -Direction Inbound -Protocol TCP -LocalPort 4022 -Action allow
New-NetFirewallRule -DisplayName "SQL Debugger/RPC" -Direction Inbound -Protocol TCP -LocalPort 135 -Action allow

#Enable SQL Analysis Ports

#New-NetFirewallRule -DisplayName "SQL Analysis Services" -Direction Inbound -Protocol TCP -LocalPort 2383 -Action allow
#New-NetFirewallRule -DisplayName "SQL Browser" -Direction Inbound -Protocol TCP -LocalPort 2382 -Action allow

#Enabling related Applications

New-NetFirewallRule -DisplayName "HTTP" -Direction Inbound -Protocol TCP -LocalPort 80 -Action allow
New-NetFirewallRule -DisplayName "SQL Server Browse Button Service" -Direction Inbound -Protocol UDP -LocalPort 1433 -Action allow
New-NetFirewallRule -DisplayName "SSL" -Direction Inbound -Protocol TCP -LocalPort 443 -Action allow

#Enable Windows Firewall
Set-NetFirewallProfile -DefaultInboundAction Block -DefaultOutboundAction Allow -NotifyOnListen True -AllowUnicastResponseToMulticast True

Write-Host "done!" -ForegroundColor Green

# start the SQL installer
Try
{
if (Test-Path $SQLsource){
 write-host "about to install SQL Server 2019..." -nonewline
$fileExe =  "$SQLsource\setup.exe"
$CONFIGURATIONFILE = "$folderpath\ConfigurationFile.ini"
& $fileExe  /CONFIGURATIONFILE=$CONFIGURATIONFILE /SQLSVCACCOUNT=$SQLSVCACCOUNT /SQLSVCPASSWORD=$SQLSVCPASSWORD /AGTSVCACCOUNT=$AGTSVCACCOUNT /AGTSVCPASSWORD=$AGTSVCPASSWORD /ISSVCACCOUNT=$ISSVCACCOUNT /ISSVCPASSWORD=$ISSVCPASSWORD /SQLUSERDBDIR=$SQLUSERDBDIR /SQLUSERDBLOGDIR=$SQLUSERDBLOGDIR /SQLBACKUPDIR=$SQLBACKUPDIR /SQLTEMPDBDIR=$SQLTEMPDBDIR
 } else {
write-host "Could not find the media for SQL Server 2019..."
break
}}
catch
{write-host "Something went wrong with the installation of SQL Server 2019, aborting."
break}

Write-Host "SQL Server 2019 Installation is Complete!" -ForegroundColor Green
#configure SQL if build successful
#if($process.ExitCode -eq 0)
#{

# start the SQL Server 2019 CU10 Installation
$filepath="$folderpath\SQLServer2019-KB5001090-x64.exe"
write-host "about to install SQL Server 2019 CU10..." -nonewline
$Parms = " /quiet /IAcceptSQLServerLicenseTerms /Action=Patch /AllInstances"
$Prms = $Parms.Split(" ")
& "$filepath" $Prms | Out-Null
Write-Host "done!" -ForegroundColor Green

# start the SQL SSMS installer
$filepath="$folderpath\SSMS-Setup-ENU.exe"
write-host "about to install SSMS 18.x..." -nonewline
$Parms = " /Install /Quiet /Norestart /Logs SQLServerSSMSlog.txt"
$Prms = $Parms.Split(" ")
& "$filepath" $Prms | Out-Null
Write-Host "done!" -ForegroundColor Green

# Install dacpac
write-host "about to install dacpac framework..." -nonewline
Start-Process msiexec.exe -Wait -ArgumentList '/i "E:\Install\DacFramework.msi" /quiet /le "E:\Install\DacFramework.log"'
Write-Host "Check Logs!" -ForegroundColor Green

#}

#else
#{
 #   Write-Host "SQL install failed. Please check C:\Program Files\Microsoft SQL Server\130\Setup Bootstrap\Log\Summary.txt for further information. Exit code:" $process.ExitCode
#}


# start the SQL RS downloader
#$filepath="$folderpath\SQLServerReportingServices.exe"

# start the SQL RS installer
<#
write-host "about to install SQL Server 2019 Reporting Services..." -nonewline
$Parms = "  /IAcceptLicenseTerms True /Quiet /Norestart /Log SQLServerReportingServiceslog.txt"
$Prms = $Parms.Split(" ")
& "$filepath" $Prms | Out-Null
Write-Host "done!" -ForegroundColor Green 
#>
<#
Write-Host "Applying post installation scripts ..."
$ScriptRoot="E:\Install\SQL-Setup-Scripts"
$Version="SQL2019"
Invoke-Expression "K:\Install\ConfigureSQL.ps1 $INSTANCENAME $ScriptRoot $Version"
Write-Host "Post Installation Scripts Applied!" -ForegroundColor Green 
#>
# exit script
#write-host "Exiting script, All Done.