$server = "Infra-mgmt01"
$inventoryDB = "SQLInventory"
 

#Clean the DBList table
#Invoke-Sqlcmd -Query "TRUNCATE TABLE DBList" -Database $inventoryDB -ServerInstance $server
#Section 1 END
 
#Section 2 START
#Fetch all the instances with the respective SQL Server Version
<#
   This is an example of the result set that your query must return
   ###################################################
   # name                     #  instance            #
   ###################################################
   # server1.domain.net,45000 #  server1             #
   # server1.domain.net,45001 #  server1\MSSQLSERVER1# 
   # server2.domain.net,45000 #  server2             #
   # server3.domain.net,45000 #  server3             #
   # server4.domain.net       #  server4\MSSQLSERVER2#
   ###################################################            
#>
 
#Put in your query that returns the list of instances as described in the example result set above
$instanceLookupQuery = "SELECT name, instance FROM instances" 
$instances = Invoke-Sqlcmd -ServerInstance $server -Database $inventoryDB -Query $instanceLookupQuery
#Section 2 END
 
#Section 3 START
$dbListQuery = "
SELECT DISTINCT @@servername AS Name, dovs.logical_volume_name AS LogicalName,
dovs.volume_mount_point AS Drive,
CONVERT(INT,dovs.total_bytes/1048576.0) AS TotalSpaceInMB,
CONVERT(INT,dovs.available_bytes/1048576.0) AS FreeSpaceInMB
FROM sys.master_files mf
CROSS APPLY sys.dm_os_volume_stats(mf.database_id, mf.FILE_ID) dovs
ORDER BY FreeSpaceInMB ASC
GO
"
#Section 3 END
 
#Section 4 START
#For each instance, fetch the list of databases (along with a couple of useful pieces of information)
foreach ($instance in $instances){
   $results = Invoke-Sqlcmd -Query $dbListQuery -ServerInstance $instance.name -ErrorAction Stop -querytimeout 30
 
   #Perform the INSERT in the DBList table only if it returns information
    if($results.Length -ne 0){
 
      #Build the insert statement
      $insert = "INSERT INTO DriveSize VALUES"
      foreach($result in $results){        
         $insert += "
         (
         '"+$result['Name']+"',
         '"+$result['LogicalName']+"',
         '"+$result['Drive']+"',
         '"+$result['TotalSpaceinMB']+"',
         '"+$result['FreeSpaceinMB']+"'
            ),
         "
       }
 
      #Store the results in the table created at the beginning of this script
      Invoke-Sqlcmd -Query $insert.Substring(0,$insert.LastIndexOf(',')) -ServerInstance $server -Database $inventoryDB
   }
}
#Section 4 END
 
Write-Host "Done!"			