$server = "Infra-mgmt01"
$inventoryDB = "SQLInventory"
 

#Clean the DBList table
#Invoke-Sqlcmd -Query "TRUNCATE TABLE DBList" -Database $inventoryDB -ServerInstance $server
#Section 1 END
 
#Section 2 START
#Fetch all the instances with the respective SQL Server Version
<#
   This is an example of the result set that your query must return
   ###################################################
   # name                     #  instance            #
   ###################################################
   # server1.domain.net,45000 #  server1             #
   # server1.domain.net,45001 #  server1\MSSQLSERVER1# 
   # server2.domain.net,45000 #  server2             #
   # server3.domain.net,45000 #  server3             #
   # server4.domain.net       #  server4\MSSQLSERVER2#
   ###################################################            
#>
 
#Put in your query that returns the list of instances as described in the example result set above
$instanceLookupQuery = "SELECT name, instance FROM instances" 
$instances = Invoke-Sqlcmd -ServerInstance $server -Database $inventoryDB -Query $instanceLookupQuery
#Section 2 END
 
#Section 3 START
$dbListQuery = "
SELECT SERVERPROPERTY ('SERVERNAME') AS InstanceName, 
        SERVERPROPERTY('Edition') AS SQLEdition,
        SERVERPROPERTY('Collation') AS ServerCollation,
        SERVERPROPERTY('ProductLevel') AS ProductLevel,
        SERVERPROPERTY('ProductVersion') AS PatchLevel
        ,db.name AS DatabaseName
        ,db.Collation_name AS DatabaseCollation,
        CONVERT(INT,SUM(size)*8.0/1024) AS 'size',
        db.Compatibility_Level, 
        db.Recovery_Model_desc,
        db.state_desc AS 'state',
        suser_sname(db.owner_sid) AS 'owner',
        GETDATE() as 'timestamp'
        FROM        sys.databases db
        JOIN        sys.master_files mf
ON          db.database_id = mf.database_id
where db.database_id>4
GROUP BY    db.name, db.state_desc,db.owner_sid,db.Collation_name,db.Compatibility_Level,db.Recovery_Model_desc
ORDER BY    db.name
"
#Section 3 END
 
#Section 4 START
#For each instance, fetch the list of databases (along with a couple of useful pieces of information)
foreach ($instance in $instances){
   $results = Invoke-Sqlcmd -Query $dbListQuery -ServerInstance $instance.name -ErrorAction Stop -querytimeout 30
 
   #Perform the INSERT in the DBList table only if it returns information
    if($results.Length -ne 0){
 
      #Build the insert statement
      $insert = "INSERT INTO DBCollection VALUES"
      foreach($result in $results){        
         $insert += "
         (
         '"+$result['InstanceName']+"',
         '"+$result['SQLEdition']+"',
         '"+$result['ServerCollation']+"',
         '"+$result['ProductLevel']+"',
         '"+$result['PatchLevel']+"',
         '"+$result['DatabaseName']+"',
         '"+$result['DatabaseCollation']+"',
         "+$result['size']+",
         "+$result['Compatibility_Level']+",
         '"+$result['Recovery_Model_desc']+"',
         '"+$result['state']+"',
         '"+$result['owner']+"',
            '"+$result['timestamp']+"'
            ),
         "
       }
 
      #Store the results in the table created at the beginning of this script
      Invoke-Sqlcmd -Query $insert.Substring(0,$insert.LastIndexOf(',')) -ServerInstance $server -Database $inventoryDB
   }
}
#Section 4 END
 
Write-Host "Done!"			