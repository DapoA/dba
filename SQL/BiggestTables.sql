--Table Sizes
select top 10 schema_name(tab.schema_id) + '.' + tab.name as [table], cast(sum(spc.used_pages * 8)/1024.00 as numeric(36, 2)) as used_mb, 
cast(sum(spc.total_pages * 8)/1024.00 as numeric(36, 2)) as allocated_mb 
from sys.tables tab 
join sys.indexes ind 
on tab.object_id = ind.object_id 
join sys.partitions part 
on ind.object_id = part.object_id and ind.index_id = part.index_id 
join sys.allocation_units spc 
on part.partition_id = spc.container_id 
group by schema_name(tab.schema_id) + '.' + tab.name 
order by sum(spc.used_pages) desc;

--Row Count
SELECT SCHEMA_NAME(schema_id) AS [SchemaName],
[Tables].name AS [TableName],
SUM([Partitions].[rows]) AS [TotalRowCount]
FROM sys.tables AS [Tables]
JOIN sys.partitions AS [Partitions]
ON [Tables].[object_id] = [Partitions].[object_id]
AND [Partitions].index_id IN ( 0, 1 )
-- WHERE [Tables].name = N'name of the table'
GROUP BY SCHEMA_NAME(schema_id), [Tables].name;