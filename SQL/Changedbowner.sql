DECLARE @dbname VARCHAR (100)
DECLARE @sql VARCHAR (255)
SET NOCOUNT ON

select [name] into #Work from sys.databases where database_id > 4  and state= 0

select @dbname = min(name) from #Work

while @dbname is not null
begin

SET @sql = 'USE ' +'[' + @dbname +']' + CHAR (13) + 'exec sp_changedbowner sa ' + CHAR (13)
EXEC (@sql)
    select @dbname = min(name) from #Work where [name] > @dbname
end
DROP TABLE #Work