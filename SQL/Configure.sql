use master;
EXEC sp_configure 'cost threshold for parallelism', 100; --48.68%
RECONFIGURE;
EXEC sp_configure 'optimize for ad hoc workloads', 1;
RECONFIGURE;
EXEC sp_configure 'max degree of parallelism', 4; --08:45
RECONFIGURE;

EXEC sp_configure 'cost threshold for parallelism'
EXEC sp_configure 'optimize for ad hoc workloads'
EXEC sp_configure 'max degree of parallelism'


 SELECT @@Servername,cpu_count AS [Logical CPU Count], hyperthread_ratio AS Hyperthread_Ratio,
cpu_count/hyperthread_ratio AS Physical_CPU_Count,
physical_memory_kb/1048576 AS Physical_Memory_in_MB,
sqlserver_start_time, affinity_type_desc -- (affinity_type_desc is only in 2008 R2)
FROM sys.dm_os_sys_info

--backup database --08:45
