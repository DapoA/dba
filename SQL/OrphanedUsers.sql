USE MASTER
GO 
SELECT name as SQLServerLogIn,SID as SQLServerSID FROM sys.syslogins
WHERE [name] = 'trandev1sql'
GO

USE cube_dev1_transform
GO 
SELECT name DatabaseID,SID as DatabaseSID FROM sysusers
WHERE [name] = 'trandev1sql'
GO

USE cube_dev1_transform
GO

sp_change_users_login @Action='Report'
GO

USE cube_dev1_transform;
sp_change_users_login @Action='update_one', 
@UserNamePattern='trandev1sql', 
@LoginName='trandev1sql'
GO

----Command to map an orphaned user
EXEC sp_change_users_login 'Auto_Fix', 'trandev1sql';

EXEC sp_change_users_login 'Auto_Fix', 'trandev1sql', null,'pwd'
