--CREATE NONCLUSTERED INDEX idx_IngestionId ON IngestedData(IngestionId)

--sp_help RDI_TAXMAPPINGS
USE RdiArticleIngestor;
UPDATE STATISTICS IngestedData (idx_IngestionId) with fullscan;
UPDATE STATISTICS IngestedData (PK_IngestionBody) with fullscan;

UPDATE STATISTICS IssueLog (idx_IssueLog_IngestedDataid) with fullscan;
UPDATE STATISTICS IssueLog (PK_IssueLog) with fullscan;

UPDATE STATISTICS IngestionProcess (idx_IngestionProces_SpiderId) with fullscan;
UPDATE STATISTICS IngestionProcess (idx_IngestionProcess_IngestedDataId) with fullscan;
UPDATE STATISTICS IngestionProcess (idx_IngestionProcess_MappedDate) with fullscan;
UPDATE STATISTICS IngestionProcess (idx_IngestionProcess_SectionSpider) with fullscan;
UPDATE STATISTICS IngestionProcess (idx_IngestionProcess_SectionSpider_Id) with fullscan;
UPDATE STATISTICS IngestionProcess (PK_IngestionProcess) with fullscan;

UPDATE STATISTICS PipeStatus (idx_PipeStatus_IngestedDataId) with fullscan;
UPDATE STATISTICS PipeStatus (idx_PipeStatus_IngestionId) with fullscan;
UPDATE STATISTICS PipeStatus (idx_PipeStatus_IngestionProcessId) with fullscan;
--UPDATE STATISTICS PipeStatus (IDX_PipeStatus_ValidationRetry_INC_ALL) with fullscan; --Not in Prod
UPDATE STATISTICS PipeStatus (PK_PipeStatus) with fullscan;

UPDATE STATISTICS Ingestions (IDX_EndPointIdContinuousSequenceId) with fullscan;
UPDATE STATISTICS Ingestions (IDX_EndPointIdSequenceId) with fullscan;
UPDATE STATISTICS Ingestions (idx_Ingestions_EndPointId) with fullscan;
UPDATE STATISTICS Ingestions (idx_Ingestions_IngestedDataId) with fullscan;
UPDATE STATISTICS Ingestions (PK_Ingestion) with fullscan;

UPDATE STATISTICS translation (idx_Translation_Updated) with fullscan;
UPDATE STATISTICS translation ([idx-Translation-IngestedDataId]) with fullscan;
UPDATE STATISTICS translation (PK_Translation) with fullscan;

USE RegInsight;
UPDATE STATISTICS RDI_MAPPINGS (IDX_CONCEPTID_REGITEMID) with fullscan;
UPDATE STATISTICS RDI_MAPPINGS (IDX_MAPPING_PARENTID) with fullscan;
UPDATE STATISTICS RDI_MAPPINGS (IDX_MAPPINGS_BOOK_ID) with fullscan;
UPDATE STATISTICS RDI_MAPPINGS (IDX_MAPPINGS_CONTAINERID) with fullscan;
UPDATE STATISTICS RDI_MAPPINGS (IDX_MAPPINGS_REGITEM_ID_INC_CONCEPT_ID) with fullscan;
UPDATE STATISTICS RDI_MAPPINGS (PK__RDI_MAPP__9476A0524CDF961A) with fullscan;
UPDATE STATISTICS RDI_MAPPINGS (UQ_PK__RDI_MAPP__9476A0524CDF961A_33A072D1) with fullscan;

UPDATE STATISTICS RDI_TAXMAPPINGS (IDX_TAXMAPPING_BOOKID) with fullscan;
UPDATE STATISTICS RDI_TAXMAPPINGS (PK__RDI_TAXM__8C79103695CE1098) with fullscan;
UPDATE STATISTICS RDI_TAXMAPPINGS (UQ_PK__RDI_TAXM__8C79103695CE1098_7C186ED3) with fullscan;

UPDATE STATISTICS RDI_CACHE_BOOK_SUBSCRIPTION (IDX_CACHEBOOKSUBS_BOOKID) with fullscan;
--UPDATE STATISTICS RDI_CACHE_BOOK_SUBSCRIPTION (IDX_CBS_SUB_BOOK_VO) with fullscan;  --Not in Prod
UPDATE STATISTICS RDI_CACHE_BOOK_SUBSCRIPTION (PK_RDI_CACHE_BOOK_SUBSCRIPTION) with fullscan;

UPDATE STATISTICS RDI_REGULATIONBOOKS (IDX_Books_IbId_State_Class_Root_INC_AllBookColumns) with fullscan;
UPDATE STATISTICS RDI_REGULATIONBOOKS (IDX_BOOKS_State_class_root_INC_IbId) with fullscan;
UPDATE STATISTICS RDI_REGULATIONBOOKS (IDX_REGULATIONBOOKS_CLASS_ORDINAL) with fullscan;
UPDATE STATISTICS RDI_REGULATIONBOOKS (IDX_REGULATIONBOOKS_DOCUMENTCLASS) with fullscan;
UPDATE STATISTICS RDI_REGULATIONBOOKS (IDX_REGULATIONBOOKS_EXTERNALID) with fullscan;
UPDATE STATISTICS RDI_REGULATIONBOOKS (IDX_REGULATIONBOOKS_SOURCEID) with fullscan;
UPDATE STATISTICS RDI_REGULATIONBOOKS (IDX_REGULATIONBOOKS_STATE_CLASS_NAME_ORDINAL_RRITEM_IB_SOURCEID) with fullscan;
UPDATE STATISTICS RDI_REGULATIONBOOKS (IDX_ROOT_REGULATION_ITEM) with fullscan;
UPDATE STATISTICS RDI_REGULATIONBOOKS ([NonClusteredIndex-20200501-122909]) with fullscan;
UPDATE STATISTICS RDI_REGULATIONBOOKS (PK__RDI_REGU__054D27E40180A010) with fullscan;
UPDATE STATISTICS RDI_REGULATIONBOOKS ([UQ_NonClusteredIndex-20200501-122909_5A133F53]) with fullscan;
UPDATE STATISTICS RDI_REGULATIONBOOKS (UQ_PK__RDI_REGU__054D27E40180A010_1442E92D) with fullscan;


UPDATE STATISTICS [RDI_BOOKTYPES_MAPPING] ([PK__RDI_BOOKTYPES_MAPPING_ID]) WITH FULLSCAN;

/*
USE RegInsight;
exec sp_recompile RDI_REGULATIONBOOKS
exec sp_recompile RDI_MAPPINGS
exec sp_recompile RDI_CACHE_BOOK_SUBSCRIPTION

USE RdiArticleIngestor;
exec sp_recompile IngestionProcess;
exec sp_recompile IngestedData;
exec sp_recompile IssueLog;
exec sp_recompile Ingestions;
exec sp_recompile PipeStatus;
exec sp_recompile translation;
*/