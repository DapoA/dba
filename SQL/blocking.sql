SELECT session_id,
    blocking_session_id,
    start_time,
    status,
    command,
    DB_NAME(database_id) as [database],
    wait_type,
    wait_resource,
    wait_time,
    open_transaction_count, 
    plan_handle 
FROM SYS.DM_EXEC_REQUESTS
WHERE session_id > 49


SELECT DER.session_id, DES.login_name, DES.program_name
FROM sys.dm_exec_requests AS DER
  JOIN sys.databases AS DB
    ON DER.database_id = DB.database_id
  JOIN sys.dm_exec_sessions AS DES
    ON DER.session_id = DES.session_id
WHERE DB.name = 'cube_prod';		

SELECT session_id, open_transaction_count
FROM sys.dm_exec_sessions
WHERE open_transaction_count > 0;	

USE [master];
SELECT 'KILL ' + CAST(session_id AS VARCHAR(10)) AS 'SQL Command', 
login_name as 'Login'
FROM sys.dm_exec_sessions
WHERE is_user_process = 1
AND database_id = DB_ID('cube_prod') --specify database name
AND program_name = '.Net SqlClient Data Provider';

sp_who2