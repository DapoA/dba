SELECT 
'INSERT ServerDatabases (ServerName, [DatabaseName], create_date,	compatibility_level, [LogicalName],Path,InitialSize_MB) values ('''
+ @@Servername+''''
+ ','''+db.name+''''
+ ','''+CAST(create_date AS VARCHAR)+''''
+ ','''+CAST(compatibility_level AS VARCHAR)+''''
+ ','''+ mf.name+''''
+ ','''+mf.physical_name+''''
+ ','''+cast(CAST(
        (mf.Size * 8
        ) / 1024.0 AS DECIMAL(18, 1)) AS VARCHAR) +''')'
FROM 
     sys.master_files AS mf
     INNER JOIN sys.databases AS db ON
            db.database_id = mf.database_id
where db.database_id>4 --and mf.type_desc='ROWS'