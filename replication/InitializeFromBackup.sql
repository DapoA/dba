
--Drop Subscription from Publisher
DECLARE @publication AS sysname;
DECLARE @subscriber AS sysname;
DECLARE @publisher AS sysname;
DECLARE @distributor AS sysname;
SET @publication = N'Reginsight_Pub';
SET @subscriber = 'CUBEUAT1SQL02';
SET @publisher = 'CUBEPROD1SQL02'
SET @distributor = 'CUBEUAT1SQL02'
USE RegInsight
EXEC sp_dropsubscription 
  @publication = @publication, 
  @article = N'all',
  @subscriber = @subscriber;
GO

--Create Subscription from Publisher initialisation using backup
USE RegInsight
GO
EXEC sp_addsubscription 
@publication = 'RegInsight_Pub', 
@subscriber = 'CUBEUAT1SQL02', 
@destination_db = 'RegInsight',
@sync_type = 'initialize with backup',
@backupdevicetype ='disk',
@backupdevicename = '\\cubeprod1fs01\Shares\LIVE\RegInsight\RegInsight.bak'

EXEC sp_addpullsubscription_agent 
  @publisher = 'CUBEPROD1SQL02', 
  @publisher_db = 'RegInsight', 
  @publication = 'RegInsight_Pub', 
  @distributor = 'CUBEUAT1SQL02';
GO