-- on the publisher database
select allow_initialize_from_backup, min_autonosync_lsn,* from syspublications

--on distribution database
select min_autonosync_lsn,* from distribution.dbo.mspublications

select subscription_seqno,publisher_seqno,ss_cplt_seqno,* from distribution.dbo.mssubscriptions

/*
Link: https://repltalk.com/2019/01/03/deep-dive-on-initialize-from-backup-for-transactional-replication/
Purged Backup Watermark
When sp_addsubscrption with @backupdevicename=’backup device’, is executed, the “RESTORE HEADERONLY” 
is run against the backup and returning LASTLSN of the latest backup set.  If the backup set contains multiple backups. 
This LASTLSN is used as watermark for Distribution agent for when retrieving new transactions from msrepl_transactions.  
If the Backup\Restore processes took too long, its possible 
the Distribution cleanup job has already purged the transaction watermark.  You can use query below to validate LSN watermark.
*/

RESTORE HEADERONLY 
FROM DISK = 'G:\LOG\reginsight.bak'
 
declare @numericlsn numeric(25,0)
DECLARE 
@high4bytelsncomponent bigint,
@mid4bytelsncomponent bigint,
@low2bytelsncomponent int
 
--set the lsn here
set @numericlsn = 93000000070800001
select @high4bytelsncomponent = convert(bigint, floor(@numericlsn / 1000000000000000))
select @numericlsn = @numericlsn - convert(numeric(25,0), @high4bytelsncomponent) * 1000000000000000
select @mid4bytelsncomponent = convert(bigint,floor(@numericlsn / 100000))
select @numericlsn = @numericlsn - convert(numeric(25,0), @mid4bytelsncomponent) * 100000
select @low2bytelsncomponent = convert(int, @numericlsn)
SELECT  convert(binary(4), @high4bytelsncomponent) + convert(binary(4), @mid4bytelsncomponent) + convert(binary(2), @low2bytelsncomponent)
